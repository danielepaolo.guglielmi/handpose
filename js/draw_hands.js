const CONST_WIDTH = 640;
const CONST_HEIGHT = 360;

// Points for fingers
const fingerJoints = {
  thumb: [0, 1, 2, 3, 4],
  indexFinger: [0, 5, 6, 7, 8],
  middleFinger: [0, 9, 10, 11, 12],
  ringFinger: [0, 13, 14, 15, 16],
  pinky: [0, 17, 18, 19, 20],
};

// Infinity Gauntlet Style
const style = {
  0: { color: "yellow", size: 12 },
  1: { color: "plum", size: 4 },
  2: { color: "green", size: 8 },
  3: { color: "plum", size: 4 },
  4: { color: "plum", size: 4 },
  5: { color: "purple", size: 8 },
  6: { color: "plum", size: 4 },
  7: { color: "plum", size: 4 },
  8: { color: "plum", size: 4 },
  9: { color: "blue", size: 8 },
  10: { color: "plum", size: 4 },
  11: { color: "plum", size: 4 },
  12: { color: "plum", size: 4 },
  13: { color: "red", size: 8 },
  14: { color: "plum", size: 4 },
  15: { color: "plum", size: 4 },
  16: { color: "plum", size: 4 },
  17: { color: "orange", size: 8 },
  18: { color: "plum", size: 4 },
  19: { color: "plum", size: 4 },
  20: { color: "plum", size: 4 },
};

// FUNZIONE CHE DISEGNA LE CORDINATE DELLA MANO
function drawHandLandmarks(landmarks, ctx) {
  const keypoints = landmarks.keypoints;
  // INSERISCE LA LINEA CHE UNISCE I PUNTI INDIVIDUATI
  for (let j = 0; j < Object.keys(fingerJoints).length; j++) {
    let finger = Object.keys(fingerJoints)[j];
    //  cicla per tutte le dita
    for (let k = 0; k < fingerJoints[finger].length - 1; k++) {
      // recupera la posizione nell'array dei punti di intersezione tra le dita
      const firstJointIndex = fingerJoints[finger][k];
      const secondJointIndex = fingerJoints[finger][k + 1];

      // disegna le linee
      ctx.beginPath();
      ctx.moveTo(
        keypoints[firstJointIndex].x,
        keypoints[firstJointIndex].y
      );
      ctx.lineTo(
        keypoints[secondJointIndex].x,
        keypoints[secondJointIndex].y
      );
      ctx.strokeStyle = "plum";
      ctx.lineWidth = 2;
      ctx.stroke();
    }
  }

  // INSERISCE UN PUNTO PER OGNI CORDINATA
  for (let i = 0; i < keypoints.length; i++) {
    // Recupera le cordinate x e y per ogni punto della mano
    const x = keypoints[i].x;
    const y = keypoints[i].y;

    ctx.beginPath();
    ctx.arc(x, y, style[i]["size"], 0, 3 * Math.PI);
    ctx.fillStyle = style[i]["color"];
    ctx.fill();
  }
}

/*
LA PREIZIONE E' UN ARRAY CONTENENTE LE CORDINATE DELLE MANI
[
  {
    score: 0.8,
    Handedness: ‘Right’,
    keypoints: [
      {x: 105, y: 107, name: "wrist"},
      {x: 108, y: 160, name: "pinky_tip"},
      ...
    ]
  }
]
*/
