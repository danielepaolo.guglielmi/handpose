const videoElement = document.querySelector('video');
const canvasElement = document.querySelector('.output_canvas');
const canvasCtx = canvasElement.getContext('2d');

// CREARE ISTANZA DETENTORE
const model = handPoseDetection.SupportedModels.MediaPipeHands;
const detectorConfig = {
  runtime: 'mediapipe', // or 'tfjs'
  solutionPath: 'js/mediapipe/hands',
  modelType: 'lite'
};
let detector;

async function predictHands(videoElement) {
  if (!detector) {
    detector = await handPoseDetection.createDetector(model, detectorConfig);
  }
  // ORA POSSIAMO PREDIRE L'IMMAGINE PASSANDOGLIELA COME PARAMETRO
  const predictions = await detector.estimateHands(videoElement);
  canvasCtx.save();
  canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
  if (predictions.length > 0) {
    for (const landmarks of predictions) {
      drawHandLandmarks(landmarks, canvasCtx);
      checkHandPosition(landmarks, canvasCtx);
    }
  }
  canvasCtx.restore();
}

let limit = 5;
let count = 0;
// INIZIALIZZAZIONE CAMERA E CALLBACK DA CHIAMARE AD OGNI FRAME
const camera = new Camera(videoElement, {
  onFrame: async () => {
    if (count == limit) {
      predictHands(videoElement);
      count = 0;
    }
    count++;
  },
  width: CONST_WIDTH,
  height: CONST_HEIGHT
});
camera.start();