const gestureStrings = {
    'thumbs_up': '👍',
    'victory': '✌🏻'
};

// configure gesture estimator
// add "✌🏻" and "👍" as sample gestures
const knownGestures = [
    fp.Gestures.VictoryGesture,
    fp.Gestures.ThumbsUpGesture
];

const GE = new fp.GestureEstimator(knownGestures);

async function checkHandPosition(landmarks, ctx) {
    let keypoints = [];
    for (let i = 0; i < landmarks.keypoints.length; i++) {
        const keypoint2D = landmarks.keypoints[i];
        const keypoint3D = landmarks.keypoints3D[i];
        keypoints.push([keypoint2D.x, keypoint2D.y, keypoint3D.z]);
    }

    const est = GE.estimate(keypoints, 9);
    if (est.poseData.length > 0) {
        const data = est.poseData;
        if (data[2][1] == 'Full Curl' && data[3][1] == 'Full Curl' && data[4][1] == 'Full Curl') {
            ctx.fillStyle = "#FF000050";
            if (data[1][1] == 'No Curl' && data[1][2] == 'Vertical Up') {
                ctx.fillRect(CONST_WIDTH / 3, 0, CONST_WIDTH / 3, CONST_HEIGHT / 3);
                movePacmanWithHand("Up");
            } else if (data[1][1] == 'No Curl' && data[1][2] == 'Horizontal Right') {
                ctx.fillRect(CONST_WIDTH / 3 * 2, CONST_HEIGHT / 3, CONST_WIDTH / 3, CONST_HEIGHT / 3);
                movePacmanWithHand("Left");
            } else if (data[1][1] == 'Full Curl' &&
                (data[0][2] == 'Horizontal Left' || data[0][2] == 'Diagonal Up Left')) {
                ctx.fillRect(0, CONST_HEIGHT / 3, CONST_WIDTH / 3, CONST_HEIGHT / 3);
                movePacmanWithHand("Right");
            } else if (data[1][1] == 'Full Curl' &&
                (data[0][2] == 'Vertical Down' || data[0][2] == 'Diagonal Down Right')) {
                ctx.fillRect(CONST_WIDTH / 3, CONST_HEIGHT / 3 * 2, CONST_WIDTH / 3, CONST_HEIGHT / 3);
                movePacmanWithHand("Down");
            }
        }
    }
}
