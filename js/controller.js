let controllerElement = document.querySelector('.controller_canvas');
let controllerCtx = controllerElement.getContext('2d');

initController(controllerCtx);

function initController(ctx) {
    // IL CANVAS E' SPECCHIATO RISPETTO L'ASSE VERTICALE
    // DESTRA DEVE ESSERE DISEGNATO A SINISTRA E VICEVERSA

    // COLORO DI ROSSO I RETTANGOLI CON OPACITA' AL 30%
    ctx.fillStyle = "#FF000030";
    // DISEGNO IL RETTANGOLO DI SU
    ctx.fillRect(CONST_WIDTH / 3, 0, CONST_WIDTH / 3, CONST_HEIGHT / 3);
    // DISEGNO IL RETTANGOLO DI DESTRA
    ctx.fillRect(0, CONST_HEIGHT / 3, CONST_WIDTH / 3, CONST_HEIGHT / 3);
    // DISEGNO IL RETTANGOLO DI GIU'
    ctx.fillRect(CONST_WIDTH / 3, CONST_HEIGHT / 3 * 2, CONST_WIDTH / 3, CONST_HEIGHT / 3);
    // DISEGNO IL RETTANGOLO DI SINISTRA
    ctx.fillRect(CONST_WIDTH / 3 * 2, CONST_HEIGHT / 3, CONST_WIDTH / 3, CONST_HEIGHT / 3);

}

const controllerAreas = {
    Up: {
        x: { min: CONST_WIDTH / 3, max: CONST_WIDTH / 3 * 2 },
        y: { min: 0, max: CONST_HEIGHT / 3 }
    },
    Right: {
        x: { min: 0, max: CONST_WIDTH / 3 },
        y: { min: CONST_HEIGHT / 3, max: CONST_HEIGHT / 3 * 2 }
    },
    Down: {
        x: { min: CONST_WIDTH / 3, max: CONST_WIDTH / 3 * 2 },
        y: { min: CONST_HEIGHT / 3 * 2, max: CONST_HEIGHT }
    },
    Left: {
        x: { min: CONST_WIDTH / 3 * 2, max: CONST_WIDTH },
        y: { min: CONST_HEIGHT / 3, max: CONST_HEIGHT / 3 * 2 }
    }
}

// FUNZIONE CHE MONITORA LE POSIZIONE DELLA MANO
function checkHandPosition(landmarks, ctx) {
    const keypoints = landmarks.keypoints;
    const x = keypoints[0].x;
    const y = keypoints[0].y;

    for (let j = 0; j < Object.keys(controllerAreas).length; j++) {
        let areaName = Object.keys(controllerAreas)[j];
        let area = controllerAreas[areaName];
        if (x > area.x.min && x < area.x.max && y > area.y.min && y < area.y.max) {
            movePacmanWithHand(areaName);

            // QUANTO SOTTO VA A RICALCARE IL RIQUADRO DOVE SI TROVA IL PALMO DELLA MANO
            ctx.fillStyle = "#FF000050";
            switch (areaName) {
                case "Up":
                    ctx.fillRect(CONST_WIDTH / 3, 0, CONST_WIDTH / 3, CONST_HEIGHT / 3);
                    break;
                case "Right":
                    ctx.fillRect(0, CONST_HEIGHT / 3, CONST_WIDTH / 3, CONST_HEIGHT / 3);
                    break;
                case "Down":
                    ctx.fillRect(CONST_WIDTH / 3, CONST_HEIGHT / 3 * 2, CONST_WIDTH / 3, CONST_HEIGHT / 3);
                    break;
                case "Left":
                    ctx.fillRect(CONST_WIDTH / 3 * 2, CONST_HEIGHT / 3, CONST_WIDTH / 3, CONST_HEIGHT / 3);
                    break;
                default:
                    break;
            }
        }
    }
}